/*Integrantes
    Renata Arcos 
    Bastián Morales
*/
#include <stdio.h>
#include <pthread.h>

/* 
Este ejercicio es igual al anterior
con la diferencia en el número de hebras creadas
*/

#define NUM_THREADS 3

void *imprimir_mensaje(void *arg) {
  int id_hebra = *((int*) arg);
  printf("Hola desde la hebra %d\n", id_hebra+1);
  pthread_exit(NULL);
}

int main() {
  pthread_t hebras[NUM_THREADS];
  int ids[NUM_THREADS];

  for (int i = 0; i < NUM_THREADS; i++) {
    ids[i] = i;
    pthread_create(&hebras[i], NULL, imprimir_mensaje, (void *) &ids[i]);
    pthread_join(hebras[i], NULL);
  }


  return 0;
}
