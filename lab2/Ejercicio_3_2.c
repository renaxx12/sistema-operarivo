/*Integrantes
    Renata Arcos 
    Bastián Morales
*/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

//función para crear el último hilo
void *hilo_tres(void *arg)
{
    printf("H3 creado.\n");
    printf("H3 termina.\n");
    pthread_exit(NULL);
}

void *hilo_dos(void *arg)
{
    pthread_t h3; //aquí se llama a la función hilo_tres
    printf("H2 creado.\n");
    if (pthread_create(&h3, NULL, hilo_tres, NULL)) {
        fprintf(stderr, "Error al crear H3.\n");
        exit(1);
    }
    //printf("H2 ha creado una nueva hebra hija H3.\n");
    pthread_join(h3, NULL);
    printf("H2 termina.\n");
    pthread_exit(NULL);
}
//primero hilo hijo creado, este crea al hilo 2
void *hilo_uno(void *arg)
{
    pthread_t h2; 
    printf("H1 creado.\n");
    if (pthread_create(&h2, NULL, hilo_dos, NULL)) {
        fprintf(stderr, "Error al crear H2.\n");
        exit(1);
    }
    //printf("H1 ha creado una nueva hebra hija H2.\n");
    pthread_join(h2, NULL);
    printf("H1 termina.\n");
    pthread_exit(NULL);
}

int main()
{   //en el main solo se crea al hilo padre
    pthread_t h1;
    if (pthread_create(&h1, NULL, hilo_uno, NULL)) {
        fprintf(stderr, "Error al crear H1.\n");
        exit(1);
    }
    printf("Programa principal ha creado una nueva hebra H1.\n");
    pthread_join(h1, NULL);
    printf("Programa principal termina.\n");
    return 0;
}